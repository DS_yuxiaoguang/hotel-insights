$(document).ready(function () {
    const myChart = echarts.init(document.getElementById('main'));
    $.post('http://127.0.0.1:3000/shopping/searchDailyRates', function(searchDailyRates){
        const legendData = [];
        let xAxisData = [];
        const series = [];
        searchDailyRates.result.forEach((searchDailyRate) => {
            const {hotelId, hotelName, hotelAddress, dailyRates} = searchDailyRate;
            const dailyPrice = [];
            Object.values(dailyRates).forEach((dailyRate) => {
                const {rate: {amountBeforeTax, amountAfterTax}} = dailyRate;
                dailyPrice.push(Number(amountBeforeTax));
            });
            series.push({
                id: hotelId,
                smooth: true,
                name: hotelName,
                type: 'line',
                data: dailyPrice
            });
            legendData.push(hotelName);
            xAxisData = Object.keys(dailyRates);
        });
        var option = {
            title: {
                text: 'Rate Shopping',
                // subtext: 'Powered By Next',
                top: 15,
                left: 'center'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 0,
                top: '15%',
                data: legendData
            },
            grid: {
                left: 0,
                right: '25%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    // saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: xAxisData
            },
            yAxis: {
                type: 'value'
            },
            series: series
        };

        myChart.setOption(option);
    });

})
