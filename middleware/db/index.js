const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const endpoints = require('../config/endpoint');
const config = endpoints.endpoint !== 'dev' ? require('../../CONFIG') : require('./config');
const ca = endpoints.endpoint !== 'dev' ? [fs.readFileSync(path.join(__dirname, '../../CONFIG/CA/rds-combined-ca-bundle.pem'))] : [];

const LoggerCollection = require('../models/common/LoggerCollection');
const logger = new LoggerCollection(path.join(__filename));

require('./schema/event');
require('./schema/user');
require('./schema/apiKey');

const database = () => {
  mongoose.set('debug', false);
  mongoose.connect(
    config.dbPath(endpoints.endpoint),
    {
      sslValidate: endpoints.endpoint !== 'dev',
      sslCA: ca,
      useNewUrlParser: endpoints.endpoint !== 'dev'
    },
    (err) => {
      if(err) {
        logger.error("Mongoose connect error: ", err);
      }
    }
  );

  mongoose.connection.on('disconnected', () => {
    mongoose.connect(config.dbPath(endpoints.endpoint));
    logger.error('MongoDB is disconnected');
  });

  mongoose.connection.on('error', err => {
    logger.error('MongoDB connect error: ', err);
  });

  mongoose.connection.on('open', async () => {
    console.log('Connected to MongoDB successfully! ', config.dbPath(endpoints.endpoint).split('@')[1] || config.dbPath(endpoints.endpoint));
  })
}

module.exports = database;
