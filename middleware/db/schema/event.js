const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const endpointsSchema = new Schema(
  {
    AgencyUSB: {
      type: String
    },
    ExtranetUSB: {
      type: String
    }
  }
);
const payloadSchema = new Schema(
  {
    businessId: {
      type: String,
      required:true
    },
    type: {
      type: String,
      required:true,
      enum: ['APIKey', 'Endpoint']
    },
    version: {
      type: String,
      required:true
    },
    model: {
      type: String,
      required:true,
      enum: ['Push', 'Pull', 'BookingOnly']
    },
    apiKeyType: {
      type: String,
      required: false,
      default: 'DEFAULT',
      enum: ['DEFAULT', 'POA', 'NONPOA']
    },
    callbackUrl: {
      type: String,
      required: true
    },
    endpoints: endpointsSchema,
    validationCode: {
      type: String,
      default: '',
      required: false
    }
  }
);
const eventSchema = new Schema(
  {
    eventId: {
      type: String,
      required:true,
      unique: true
    },
    accountId: {
      type: String,
      required:true
    },
    result: {
      type: String,
      required:false,
      enum: ['Failed', 'Success']
    },
    status: {
      type: String,
      default: 'Pending',
      enum: ['Pending', 'Confirmed'],
      required: true
    },
    state: {
      type: String,
      default: ' ',
      //enum: ['Processing', 'Completed']
    },
    payload: payloadSchema,
    expireDate: String,
    errorCode: {
      type: String,
      default: '',
      required: false
    },
    errorMessage: {
      type: String,
      default: '',
      required: false
    }
  },
  {
    timestamps: true
  }
);

eventSchema.pre('save', function (next) {
  next();
});

mongoose.model('Event', eventSchema);
