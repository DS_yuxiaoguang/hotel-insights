const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const APIKeySchema = new Schema(
  {
    businessId: {
      type: String,
      required:true/*,
      unique: true*/
    },
    apiKey: {
      type: String,
      default: '',
      required: false
    },
    expireDate: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true,
      default: 'DEFAULT',
      enum: ['DEFAULT', 'POA', 'NONPOA']
    }
  },
  {
    timestamps: true
  }
);

APIKeySchema.pre('save', function (next) {
  next();
});

mongoose.model('APIKey', APIKeySchema);
