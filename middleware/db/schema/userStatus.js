const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userStatusSchema = new Schema(
  {
    status: {
      type: String,
      default: 'Deactived',
      enum: ['Deactived', 'Actived'],
      required:true
    }
  },
  {
    timestamps: true
  }
);

userStatusSchema.pre('save', function (next) {
  next();
});

mongoose.model('UserStatus', userStatusSchema);
