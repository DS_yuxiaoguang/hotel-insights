const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: {
      type: String,
      required:true
    },
    accountId: {
      type: String,
      required:true
    },
    businessId: {
      type: String,
      required:true
    },
    status: {
      type: String,
      default: 'Deactived',
      required:true
    }
  },
  {
    timestamps: true
  }
);

userSchema.pre('save', function (next) {
  next();
});

mongoose.model('User', userSchema);
