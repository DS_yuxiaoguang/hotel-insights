const path = require('path');
const moment = require('moment');
const Base64 = require('js-base64').Base64;
const crypto = require('crypto');
const Constants = require('../models/common/Constants');

const utils = {};
utils.generateAuthorization = (options) => {
  options = options || {};
  const accessToken = options.accessToken || localStorage.getItem('accessToken');
  return {headers: {"Authorization": accessToken.includes('Bearer') ? accessToken : "Bearer " + accessToken}};
};

utils.resolveURL = (prefix, suffix, options) => {
  return path.join(prefix, suffix);
};

utils.parse = function (response, result) {
  let body = {};
  if (response.statusCode == '200') {
    if (result.startsWith('{')) {
      let resultObj = utils.formatJSONString(result);
      if (resultObj && resultObj.errorCode) {
        return {
           error: {
             message: resultObj.errorMessage,
             errorCode: resultObj.errorCode
           }
        }
      }
      return resultObj
    }
    if (result.includes('</Error>')) {
      let errIndex = result.indexOf('<Error');
      let errLastIndex =  result.lastIndexOf('</Error>');
      body.error = {};
      body.error.message = result.slice(errIndex, errLastIndex);
      return body;
    }
  }
  return body;
};

utils.getAuditField = function (url, method, params, redirectHeaders, authorization) {
  let userName, companyName, identity, partnerId;
  if (redirectHeaders) {
    let _redirectHeaders = typeof redirectHeaders == 'object' ? redirectHeaders : JSON.parse(Base64.decode(redirectHeaders));
    userName = _redirectHeaders.userName;
    companyName = _redirectHeaders.companyName;
    identity = _redirectHeaders.identity;
    partnerId = _redirectHeaders.partnerId;
  }

  let auditObject = {
    'category': (Constants.ACTION_CATEGORY[method] && Constants.ACTION_CATEGORY[method].category) || 'AuditRecord',
    'user': userName,
    'source': companyName,
    'event.time': moment(moment()).utc().format('YYYY-MM-DDThh:mm:ss.SSS'),
    'context': identity,
    'action': (Constants.ACTION_CATEGORY[method] && Constants.ACTION_CATEGORY[method].action) || 'Get'
  };

  if (method) {
    auditObject['impact'] = method;
  }
  if (authorization) {
    auditObject['token'] = authorization
  }
  if (identity === 'DISTRIBUTOR') {
    auditObject['index.channel'] = partnerId
  }

  if (identity === 'SUPPLY') {
    auditObject['index.supplier'] = partnerId
  }
  auditObject['destination'] = url;
  auditObject['details'] = params;
  return auditObject
};

utils.loggerPrefFormat = function (actionAuthorization, options) {
  let str = '';
  Object.keys(actionAuthorization).forEach(keyItem => [
    str += `<${keyItem}=${actionAuthorization[keyItem]}>`
  ]);
  return str;
};

utils.formatJSONString = (request, options)=>{
  if(
    request &&
    !request.includes('html>') &&
    !request.includes('xml>') &&
    !request.includes('</') &&
    !request.includes('/>') &&
    !request.includes('script>') &&
    !request.includes('link>') &&
    (request.indexOf('{') || request.indexOf('['))
  ){
    try{
      request = request.replace(/\s+/g,'');
      return JSON.parse(request);
    }catch(e) {
    }
  }
  return request;
}

utils.encrypt = (data, key) => {
  // 注意，第二个参数是Buffer类型
  return crypto.publicEncrypt(key, Buffer.from(data));
};

utils.decrypt = (encrypted, key) => {
  // 注意，encrypted是Buffer类型
  return crypto.privateDecrypt(key, encrypted);
};
module.exports = utils;
