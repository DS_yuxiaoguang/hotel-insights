const request = require('request');
const path = require('path');
const utils = require('./utils');
const configkeeper = require('./configkeeper');

const LoggerCollection = require('../models/common/LoggerCollection');
const logger = new LoggerCollection(__filename);
const perfLogger = new LoggerCollection('perf');

const authorization = require('./authorization');

const getStreamRPC = (url, fileName, res, options = {headers:{}}) => {
  let headers = {};
  let extname = path.extname(url);
  let authorizationToken = authorization.getAuthorizationToken(url);

  if(options.headers){
    headers = Object.assign(headers, {'Authorization': options.headers['authorization'], 'Content-Type': 'application/octet-stream'})
  }

  if(!!authorizationToken){
    headers['Authorization'] = authorizationToken;
  }

  options = Object.assign(
    {},
    options,
    {
      url,
      method: 'GET',
      gzip: true,
      headers: headers
    }
  );

  request(options).on('response', function (response) {
    res.setHeader('filename', `${fileName}${extname}`);
    this.pipe(res);
  });
}

const postRPCStream = (url, fileName, params, res, options = {headers:{}}) => {
  let headers = {};
  let authorizationToken = authorization.getAuthorizationToken(url);

  if(options.headers){
    headers = Object.assign(headers, {'Authorization': options.headers['authorization'], 'Content-Type': 'application/json'})
  }

  if(!!authorizationToken){
    headers['Authorization'] = authorizationToken;
  }

  request({
    url: url,
    method: 'POST',
    headers,
    body: JSON.stringify(params)
  }).on('response', function(response) {
    res.setHeader('filename', `${fileName}.xlsx`);
    this.pipe(res);
  })
}
module.exports = {getStreamRPC, postRPCStream};
