const request = require('request');
const utils = require('./utils');

const LoggerCollection = require('../models/common/LoggerCollection');
const logger = new LoggerCollection(__filename);
const perfLogger = new LoggerCollection('perf');

const XML = (url, template, callback, options = {headers: {}}) => {

  let actionAuditField = utils.getAuditField(url, '', template.replace(/\s{2,}/g, ''), options.headers['redirect-headers'], options.headers['authorization']);

  options = {
    url: url,
    method: 'POST',
    headers: Object.assign({'Content-Type': 'text/xml'},
      {'Authorization': options.headers['authorization']}
    ),
    body: template
  };


  request(options, (err, response, body) => {
    if(err) {
      logger.error('XML URL ERROR Request:', {url: url}, 'Response', err);
      perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, 'err.msg': err, status: 'Fail'}));
      return
    }
    let _body = utils.parse(response, body);
    if(_body.error) {
      logger.error('XML URL ERROR Request:', {url: url}, 'Response', _body.error);
      perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, 'err.msg': _body.error.message, status: 'Fail'}));
    }

    perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, status: 'Success'}));
    callback(body);
  });
};


module.exports = XML;


