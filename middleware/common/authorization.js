const CONFIGKEEPER = require('../common/configkeeper');
/*
[
    {
        "url-pattern":"http://goapiproxy.derbysoft-test.com*",
        "token":"123456789012345678",
        "enabled":"true"
    },
    {
        "url-pattern":"http://goapiproxy.derbysoft-test.com*",
        "token":"123456789012345678",
        "enabled":"true"
    }
]
*/

module.exports = {
  getAuthorizationToken: function(url){
    let tokens = eval(CONFIGKEEPER.getConfig("authorization.tokens"));

    let authorizationToken =  null;
    tokens.forEach(token => {
      if(!!token['enabled']){
        if(new RegExp(token["url-pattern"]).test(url)){
          authorizationToken = token['token'];
          return true;
        }
      }
    });

    return authorizationToken;
  }
}
