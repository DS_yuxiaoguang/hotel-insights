const request = require('request');
const utils = require('./utils');
const configkeeper = require('./configkeeper');

const LoggerCollection = require('../models/common/LoggerCollection');
const logger = new LoggerCollection(__filename);
const perfLogger = new LoggerCollection('perf');

const authorization = require('./authorization');

const getURL = (url, callback, options = {headers: {}}) => {
  let _options = {
    url,
    method: 'GET',
    timeout: options.timeout || 300000,
  };

  if (options.headers) {
    Object.assign(_options, {
      headers: Object.assign(
        {'Authorization': options.headers['authorization']}
      ),
    })
  }

  if(options.redirect){
    _options.headers['Redirect-URL'] = _options.url;
    _options.url = configkeeper.getConfig('service.go.redirect');
    _options.headers = Object.assign(_options.headers, {'Redirect-Headers': JSON.stringify(_options.headers)});
  }else{
    let authorizationToken = authorization.getAuthorizationToken(url);
    if(!!authorizationToken){
      _options.headers['Authorization'] = authorizationToken;
    }
  }

  let actionAuditField = utils.getAuditField(url, '', '', options.headers['redirect-headers'], options.headers['authorization']);
  if(options.isBinaryStream === true) return request(_options).pipe(callback);

  request(_options, (err, response, body) => {
    if(err) {
      logger.error('Get URL ERROR Request:', _options, 'Response', err);
      perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, 'err.msg': err, status: 'Fail'}));
      return
    }

    let _body = utils.parse(response, body);
    if(_body.error) {
      logger.error('Get URL ERROR Request:', _options, 'Response', _body.error);
      perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, 'err.msg': _body.error.message, status: 'Fail'}));
      return callback(body);
    }
    perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, status: 'Success'}));
    callback(body);

  });
};

module.exports = getURL;
