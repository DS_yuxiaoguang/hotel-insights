const request = require("request");
let cfg = {};

module.exports = {
  getConfig: function (key) {
    return cfg[key];
  },

  loadConfig: function(url, source){
    // console.log({url, source});
    let requestData={"method": "getAll", "params":[source]};
    request({
      url: url,
      method: "POST",
      json: true,
      headers: {
        "content-type": "application/json",
      },
      body: requestData
    }, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        cfg = eval(body)['result'];
      } else {
        let errMsg = "Failed to load Remote config, ErrMsg: " + error + (!!response ? (", StatusCode=" + response.statusCode) : "");
        throw new Error(errMsg);
      }
    });
  },

  init: function(url, source){
    this.loadConfig(url, source);
    let that = this;
    setInterval(function () {
      that.loadConfig(url, source)
    }, 5000);
  },
  getConfigs: function(){
    return cfg;
  }
};
