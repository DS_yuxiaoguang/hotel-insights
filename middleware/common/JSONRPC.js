const request = require('request');
const utils = require('./utils');
const configkeeper = require('./configkeeper');

const LoggerCollection = require('../models/common/LoggerCollection');
const logger = new LoggerCollection(__filename);
const perfLogger = new LoggerCollection('perf');

const authorization = require('./authorization');

const JSONRPC = (url, method, params, callback, options = {headers:{}}) => {
  options = Object.assign({headers: {}}, options);
  let body, headers = {}, ajaxURL = url;
  if (method) {
    body = JSON.stringify({"method": method, "params": params});
  }
  if (!method.length) {
    body = JSON.stringify(params);
  }

  if(options.headers){
    headers = Object.assign(headers, {'Authorization': options.headers['authorization'], 'Content-Type': 'application/json'})
    // headers['Authorization'] = options.headers['authorization'];
  }

  if(options.redirect){
    ajaxURL = configkeeper.getConfig('service.go.redirect');
    headers['Redirect-URL'] = url;
    headers = Object.assign(headers, {'Redirect-Headers': JSON.stringify(headers)});
  }else{
    let authorizationToken = authorization.getAuthorizationToken(url);
    if(!!authorizationToken){
      headers['Authorization'] = authorizationToken;
    }
  }

  let actionAuditField = utils.getAuditField(ajaxURL, method, body, options.headers['redirect-headers'], options.headers['authorization']);
  logger.info('Request=', body);

  if (options.formData) {
    options = {
      url: ajaxURL,
      method: 'POST',
      headers: headers,
      formData: params,
      timeout: options.timeout || 300000
    };
  }else {
    options = {
      url: ajaxURL,
      method: 'POST',
      headers: headers,
      body: body,
      timeout: options.timeout || 300000
    };
  }
  request(options, (err, response, res) => {
    if(err) {
      logger.error('JSONRPC ERROR Request:', options, 'Response', err);
      perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, 'err.msg': err, status: 'Fail', response: res}));
      return callback(res);
    }
    let body = utils.parse(response, res);
    if(body && body.error) {
      logger.error('JSONRPC ERROR Request:', options, 'Response', body.error);
      perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, 'err.msg': body.error.message, status: 'Fail', response: res}));
      return callback(res);
    }

    perfLogger.perf(utils.loggerPrefFormat({...actionAuditField, status: 'Success'}));
    callback(res);
  });
};

module.exports = JSONRPC;
