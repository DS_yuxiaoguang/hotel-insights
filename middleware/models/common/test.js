// const mongoose = require('mongoose');
const fs = require('fs');
//
// const ca = [fs.readFileSync("rds-combined-ca-bundle.pem")];
// require('../../db/schema/user');
// const database = (dbPath) => {
//   mongoose.set('debug', false);
//   mongoose.connect(
//     dbPath,
//     {
//       sslValidate: true,
//       sslCA: ca,
//       useNewUrlParser: true
//     },
//     (err) => {
//       if(err) {
//         console.error("Mongoose connect error: ", err);
//       }
//     }
//   );
//
//   mongoose.connection.on('disconnected', () => {
//     mongoose.connect(dbPath);
//     console.error('MongoDB is disconnected');
//   });
//
//   mongoose.connection.on('error', err => {
//     console.error('MongoDB connect error: ', err);
//   });
//
//   mongoose.connection.on('open', async () => {
//     console.log('Connected to MongoDB successfully! ', dbPath);
//     addUser();
//   })
// }
//
// function addUser(){
//   console.log('#####Add Start#####');
//   const UserModel = mongoose.model('User');
//   const doc = {email: 'yuxiaoguang@test.com', accountId: '#*%$%$#$%', businessId: 'TEST_DB_OPEN', status: 'Actived'};
//   UserModel.create(doc, (_err, userRes) => {
//     console.log(_err, {result: userRes});
//     console.log('#####Add End#####');
//     findUser();
//   });
// }
//
// function findUser(){
//   console.log('#####Find Start#####');
//   const UserModel = mongoose.model('User');
//   const condition = {email: 'yuxiaoguang@test.com', accountId: '#*%$%$#$%', businessId: 'TEST_DB_OPEN'};
//   UserModel.findOne(condition, (_err, userRes) => {
//     console.log(_err, {result: userRes});
//     console.log('#####Find End#####');
//   });
// }
//
// database('mongodb://derbydocdb:derbydocdb2020_W4W_D8@docdb-go-console-uat.cluster-czahiwwi6rzh.us-west-2.docdb.amazonaws.com:27017/go-console?ssl=true&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=falseCA');
//


const utils = require('../../common/utils');
const keys = require('./keys');

const plainText = `eyJ1c2VyTmFtZSI6Inl1eGlhb2d1YW5nQGRlcmJ5c29mdC5jb20iLCJjb21wYW55TmFtZSI6IkdPIEJvb2tpbmcgUHJvZHVjdCBUZWFtIiwidXNlcklkIjoiVVZwRVYyb1dkcjRhd0V6TCIsImZ1bGxOYW1lIjoieXV4aWFvZ3VhbmciLCJpZGVudGl0eSI6IkRJU1RSSUJVVE9SIiwicGFydG5lcklkIjoiR09CT09LSU5HIn0=`;
const crypted = utils.encrypt(plainText, keys.pubKey); // 加密
const decrypted = utils.decrypt(crypted, keys.privKey); // 解密

const ws1 = fs.createWriteStream('original.txt');
const ws2 = fs.createWriteStream('encrypt.txt');
ws1.write(plainText);
ws2.write(crypted);

console.log(crypted); // binary data
console.log(decrypted.toString()); // 你好，DerbySoft
