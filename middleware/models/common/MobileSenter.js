const showapiSdk = require('showapi-sdk');
const mobilePhones = [
  '18221572412'
];

const SECRET = '18af85bb587e420898680543213ed98c';
const APPID = '107720';

const PROJECT_NAME = 'GO BOOKING';
const NOTE_TEMPLATE_NUMBER = 'T170317005329';

class MobileSenter {
  constructor(mobileSenter ={}) {
    this.errorMsg = mobileSenter.errorMsg;
    this.appId = APPID;
    this.secret = SECRET;
  }
  send(){
    let {appId, secret} = this;
    showapiSdk.setting({
      url:"http://route.showapi.com/28-1",//你要调用的API对应接入点的地址,注意需要先订购了相关套餐才能调
      appId: appId,//你的应用id
      secret: secret,//你的密钥
      timeout:5000,//http超时设置
    });

    const request = showapiSdk.request();
    let content = {
      projectName: PROJECT_NAME,
      errorConent: this.errorMsg
    };

    request.appendText('content', JSON.stringify(content));
    request.appendText('tNum', NOTE_TEMPLATE_NUMBER);
    request.appendText('big_msg','1');

    mobilePhones.forEach((mobilePhone) => {
      request.appendText('mobile', mobilePhone);
      request.post(function(data){
        console.log("data", data)
      })
    })
  }
}

module.exports = MobileSenter;
