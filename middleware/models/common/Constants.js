const { externetService } = require('../../config/index');
const STATIC_CONSTANTS = {
  'APP_ID': externetService.identifyCode, //,'E8N6VjMYBDK8',//'goTestId',
  'GRANT_TYPE': 'authorization_code',
  'SESSION_TYPE': 'jwt',
  'TOKEN_TYPE': 'bearer'
};

const ACTION_CATEGORY = {
  'searchSupplierConnections': {action: 'Search', category: 'SupplierConnections'},
  'searchDistributorConnections': {action: 'Search', category: 'DistributorConnections'},
  'listWaitApproveConnections': {action: 'Search', category: 'WaitApproveConnection'},
  'approveConnection': {action: 'Add', category: 'Connection'},
  'searchDistributors': {action: 'Search', category: 'Distributors'},
  'approvePartner': {action: 'Add', category: 'Partner'},
  'deletePartner': {action: 'Delete', category: 'Partner'},
  'getPartnerState': {action: 'Search', category: 'Partner'},
  'updatePartnerSetting': {action: 'Update', category: 'PartnerSetting'},
  'searchPartners': {action: 'Search', category: 'Partners'},
  'searchSuppliers': {action: 'Search', category: 'Suppliers'},
  'getSupplierCandidate': {action: 'Search', category: 'SupplierCandidate'},
  'listConnectionSettings': {action: 'Search', category: 'SupplierConnections'},
  'getConnectionSetting': {action: 'Search', category: 'SupplierConnection'},
  'updateConnectionSetting': {action: 'Update', category: 'SupplierConnection'},
  'submitConnectionSetting': {action: 'Update', category: 'SupplierConnection'},
  'distributorSummary': {action: 'Search', category: 'DistributorSummary'},
  'distributorHistogram': {action: 'Search', category: 'DistributorHistogram'},
  'searchDistributorHotels': {action: 'Search', category: 'DistributorHotels'},
  'updateDistributorHotels': {action: 'Update', category: 'DistributorHotels'},
  'listDistributorReport': {action: 'Search', category: 'DistributorTransactionReport'},
  'listDistributorErrorReport': {action: 'Search', category: 'DistributorErrorReport'},
  'listDailyReservationReport': {action: 'Search', category: 'DistributorDailyReservationReport'},
  'setupDistributor': {action: 'Update', category: 'Distributor'},
  'getDistributorSetting': {action: 'Search', category: 'DistributorSetting'},
  'updateDistributorSetting': {action: 'Update', category: 'DistributorSetting'},
  'submitDistributorSetting': {action: 'Update', category: 'DistributorSetting'},
  'updateDistributorApis': {action: 'Update', category: 'DistributorApis'},
  'unlockDistributorApis': {action: 'Update', category: 'DistributorApis'},
  'updateDistributorEndpoints': {action: 'Update', category: 'DistributorEndpoints'},
  'getSupplierSetting': {action: 'Search', category: 'SupplierSetting'},
  'updateSupplierSetting': {action: 'Update', category: 'SupplierSetting'},
  'submitSupplierSetting': {action: 'Update', category: 'SupplierSetting'},
  'updateSupplierApis': {action: 'Update', category: 'SupplierApis'},
  'unlockSupplierApis': {action: 'Update', category: 'SupplierApis'},
  'getConnections': {action: 'Search', category: 'DistributorConnections'},
  'getDistributorCandidate': {action: 'Search', category: 'DistributorConnections'},
  'listSupplierErrorReport': {action: 'Search', category: 'SupplierErrorReport'},
  'listSupplierReport': {action: 'Search', category: 'SupplierTransactionReport'},
  'runCertification': {action: 'Update', category: 'Certification'},
  'checkCertification': {action: 'Search', category: 'Certification'},
  'getUserInfo': {action: 'Search', category: 'UserInfo'},

  'sendEvent': {action: 'Add', category: 'LoginRecord'},
  'sendConnectionRequest': {action: 'Add', category: 'ConnectionRequest'},
  'sendApproveRequestEvent': {action: 'Add', category: 'ApproveRequestEvent'},
  'sendRequestEvent':{action: 'Add',  category: 'ConnectionRequestEvent'},
  'sendActivityRequestEvent': {action: 'Add', category: 'ActivityRequestEvent'},

  'applyService': {action: 'Add', category: 'Partner'},
  'triggerPush': {action: 'Add'},
  'triggerHotelSync': {action: 'Add'},
  'download': {action: 'Download'},
  'summaryMetric': {action: 'Search', category: 'SupplierSummary'},
  'histogram': {action: 'Search', category: 'SupplierHistogram'},
  'searchReservations': {action: 'Search', category: 'Reservations'},
  'getReservation': {action: 'Search', category: 'Reservation'},
  'getReservationTimelines': {action: 'Search', category: 'ReservationTimelines'},
  'getReservationStreams': {action: 'Search', category: 'ReservationStreams'},
  'searchSupplierHotels': {action: 'Search', category: 'SupplierHotels'},
  'getSupplierHotel': {action: 'Search', category: 'SupplierHotel'},

  'searchHotelFee': {action: 'Search', category: 'HotelFee'},
  'deleteHotelFee': {action: 'Delete', category: 'HotelFee'},

  'addDistributorRateRule': {action: 'Add', category: 'DistributorRateRule'},
  'searchDistributorRateRule': {action: 'Search', category: 'DistributorRateRule'},
  'deleteDistributorRateRule': {action: 'Delete', category: 'DistributorRateRule'}
};

const EMAILES = [
  'hulinhai@derbysoft.net',
  'yuxiaoguang@derbysoft.net'
];

module.exports = {
  STATIC_CONSTANTS,
  ACTION_CATEGORY,
  EMAILES
};
