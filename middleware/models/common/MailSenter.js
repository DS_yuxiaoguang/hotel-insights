const nodeMailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const logger = require('log4js').getLogger(path.join(__filename));
const ejs = require('ejs');

const Constants = {
  'MAIL': {
    'SERVICE': 'Gmail',
    'HOST': 'smtp.gmail.com',
    'PORT': 587,
    'USER': 'go.noreply@derbysoft.net',
    'PASSWORD': 'Hp3k4R0lkYaU',
    'SECURE': false, // true for 465, false for other ports
  }
};
class MailSenter {
  constructor(mailSenter = {}) {
    this.to = mailSenter.to;
    this.cc = mailSenter.cc;
    this.subject = mailSenter.subject;
    this.htmlHeader = mailSenter.htmlHeader;
    this.htmlContent = mailSenter.htmlContent;
    this.htmlBtnDesc = mailSenter.htmlBtnDesc;
    this.htmlBtnText = mailSenter.htmlBtnText;
    this.htmlBtnLink = mailSenter.htmlBtnLink;
    this.htmlContentTips = mailSenter.htmlContentTips;
  }

  send(callback, options){
    const {to, cc, subject, htmlHeader, htmlContent, htmlBtnDesc, htmlBtnText, htmlBtnLink, htmlContentTips} = this;

    let mailTemplate = fs.readFileSync(path.join(__dirname, '../../public/email_template/template.html'));
    let html = mailTemplate.toString();
    html = ejs.render(html, {content: {subject, htmlHeader, htmlContent, htmlBtnDesc, htmlBtnText, htmlBtnLink, htmlContentTips}});
    const {SERVICE: service, HOST: host, PORT: port, USER: user, PASSWORD: pass, SECURE: secure} = Constants.MAIL;
    let transporter = nodeMailer.createTransport({service, host, port, secure, pool: true, auth: {user, pass}});
    let mailOptions = {from: user, to, cc, subject, html};
    transporter.sendMail(mailOptions, function(error, info){
      transporter.close();
      if(error) return logger.error(error);

      logger.info('Mail Message request : ', htmlContent);
      logger.info('Mail Sent To : [ '+ to + ' ], ' + info.response);

      callback && callback(error, info);
    });
  }
}

module.exports = MailSenter;
