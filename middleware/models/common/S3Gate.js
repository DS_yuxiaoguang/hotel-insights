const AWS = require('aws-sdk');
var uuid = require('node-uuid');
const path = require('path');

const bucketConstant = {
  REGION: 'ap-southeast-1',
  BUCKET_NAME: 'derby-nuke-app-ap-southeast-1',
  KEY_NAME_PREFIX: '/next'
};

class S3Gate {
  constructor(s3Gate) {
    this.s3 = new AWS.S3({region: bucketConstant.REGION});
    this.bucketName = bucketConstant.BUCKET_NAME;
    this.keyName = [bucketConstant.KEY_NAME_PREFIX, s3Gate.keyName].join('/');
    this.bodyParams = s3Gate.bodyParams;

  }
  createBucket(){
    var bucketName = 'node-sdk-sample-' + uuid.v1();
    var keyName = 'hello_world.txt';

    this.s3.createBucket({Bucket: bucketName}, function() {
      var params = {Bucket: bucketName, Key: keyName, Body: 'Hello World!'};
      this.s3.putObject(params, function(err, data) {
        if (err)
          console.log(err)
        else
          console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
      });
    });
  }
  listBuckets(){
    const params = {};
    this.s3.listBuckets(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else console.log(data);           // successful response
    });
  }
  putObject(callback, options){
    const params = {
      Bucket: this.bucketName,
      Key: this.keyName,
      Body: this.bodyParams
    };
    this.s3.putObject(params, function(err, data) {
      if (err){
        console.log('S3 Doc Put Request : ', JSON.stringify(params));
        console.log('Put S3 Doc Put Request Failed, Error is : ', err);
        callback({err});
      } else{
        callback(data);
        console.log('Successfully uploaded data to : ', params.Bucket + "/" + params.Key);
      }
    });
  }
  getObject(callback, options){
      const params = {
          Bucket: this.bucketName,
          Key: this.keyName
      };

      this.s3.getObject(params, function(err, data) {
          if (err){
              console.log('S3 Doc Get Request : ', JSON.stringify(params));
              console.log('Put S3 Doc Get Request Failed, Error is : ', err);
              callback({err});
          } else{
              let objectData = data.Body.toString('utf-8');
              callback(JSON.parse(objectData));
              console.log('Successfully Get data from : ', params.Bucket + "/" + params.Key);
          }
      });
  }
}

module.exports = S3Gate;
