const log4js = require('log4js');
const log4jsJSON = require('../../log4js.json');

const MailSenter = require('./MailSenter');
const endpoint = require('../../config/endpoint');
const externetService = JSON.parse(endpoint.externetService);
const Constants = require('./Constants');

const EMAILES = Constants.EMAILES;

class LoggerCollection {
  constructor(loggerTagName) {
    this.logger = null;
    this.configure();
    this.getLogger(loggerTagName)
  }

  configure() {
    log4js.configure(log4jsJSON);
  }

  connectLogger(logger, options) {
    return log4js.connectLogger(logger, options)
  }

  getLogger(loggerTagName) {
    this.logger = log4js.getLogger(loggerTagName);
  }

  error(requestTagName, requestBody, responseTagName, responseBody) {
    let errorMessage =  requestTagName + (requestBody ? JSON.stringify(requestBody) : '');
    if (responseTagName) {
      errorMessage += '\r\n' + responseTagName;
      errorMessage += (responseBody && responseBody.toString().includes('Error')) ? responseBody.toString() : JSON.stringify(responseBody)
    }
    this.logger.error(errorMessage);
    this._pushEmailMessage(errorMessage);
  }

  info(infoTagName, ...args) {
    this.logger.info(infoTagName, ...args);
  }

  trace(traceTagName, ...args) {
    this.logger.trace(traceTagName, ...args);
  }

  warn(warnTagName, ...args) {
    this.logger.warn(warnTagName, ...args);
  }

  fatal(fatalTagName, ...args) {
    this.logger.fatal(fatalTagName, ...args);
  }

  perf(perfStr) {
    if (!perfStr) return;
    this.logger.info(perfStr);
  }

  _pushEmailMessage(errorMsg) {
    let emailObj = {};
    let {singleSignSystemURL, identifyCode} = externetService;

    emailObj.cc = '';
    emailObj.subject = `[${endpoint.endpoint.toUpperCase()}] GO Console Web Error Message`;
    emailObj.htmlHeader = '错误异常提醒';
    emailObj.htmlContent = errorMsg;
    emailObj.htmlBtnDesc = '点击下面按钮, 前往查看';
    emailObj.htmlBtnText = '前往查看';
    emailObj.htmlBtnLink  = [singleSignSystemURL, '/nonauth/login?appId=', identifyCode].join('');

    EMAILES.forEach(email => {
      emailObj.to = email;
      const mailSenter = new MailSenter({...emailObj});
      mailSenter.send();
    })
  }
}

module.exports = LoggerCollection;
