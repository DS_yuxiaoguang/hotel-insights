const puppeteer = require('puppeteer');
async function getFloorDailyRatesFromOTA(websiteURL) {
    const cookie = {
        name: 'cticket',
        value: '60FA278C323F0C3ECEFD3A9933181669DF790538A0CD73A3AE0AFE4E896DB5C1',
        domain: '.ctrip.com',
        url: 'https://hotels.ctrip.com/',
        path: '/',
        httpOnly: false,
        secure: false
    };

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setCookie(cookie)
    // browser.userAgent().then(agent => console.log({agent}))

    await page.goto(websiteURL);

    // await page.cookies().then(cookie => console.log({cookie}))

    // Wait for the results page to load and display the results.
    const salecardRooms = '.detail-head-price_container .detail-head-price_price_box .detail-head-price_price_contain .detail-head-price_price';
    await page.waitForSelector(salecardRooms);

    // Extract the results from the page.
    const salecardRoomsContents = await page.evaluate(salecardRooms => {
        const anchors = Array.from(document.querySelectorAll(salecardRooms));
        return anchors.map(anchor => {
            const title = anchor.textContent.split('|')[0].trim();
            return `${title}`;
        });
    }, salecardRooms);

    // await page.screenshot({path: 'example.png'});
    console.log(salecardRoomsContents.join('\n'));
    await browser.close();
}
const websiteURL = 'https://hotels.ctrip.com/hotels/detail/?hotelId=2987364&checkIn=2020-12-12&checkOut=2020-12-13&cityId=2&minprice=&mincurr=&adult=1&children=0&ages=&crn=1&curr=&fgt=&stand=&stdcode=&hpaopts=&mproom=&ouid=&shoppingid=&roomkey=&highprice=-1&lowprice=0&showtotalamt=&hotelUniqueKey=&relatedate='

getFloorDailyRatesFromOTA(websiteURL)

