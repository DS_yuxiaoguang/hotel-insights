const fs = require('fs');
const path = require('path');
const S3Gate = require('./common/S3Gate.js');
const puppeteer = require('puppeteer');

class Home{
  constructor() {
  }
  renderHomeWidget(req, res, options){
    const accessPath = path.join(__dirname, '../web/index.html');
    fs.stat(accessPath, function (err, stats) {
      if(!stats || stats.isDirectory()){
        console.log('Missing access file!');
        res.render('404');
        res.end();
        return;
      }
      fs.readFile(accessPath, function (err, data) {
        if(err) throw err;
        res.writeHead(200, {'Content-Type': 'text/html;charset=UTF-8'});
        res.write(data);
        res.end();
      });
    })
  }
  updateHotel(hotelId, hotelName, hotelAddress, dailyRates, options){
    return new Promise((resolve => {
        this
            .getHotel(hotelId)
            .then((getHotelResult) => {
                const {dailyRates: getdailyRates} = getHotelResult;
                dailyRates = getdailyRates ? {...getdailyRates, ...dailyRates} : {...dailyRates};
                const hotelDoc = {hotelId, hotelName, hotelAddress, dailyRates, products: {}};
                const s3Gate = new S3Gate({keyName: ['hotel/', hotelId, '.json'].join(''), bodyParams: JSON.stringify(hotelDoc)});//TODO://
                s3Gate.putObject((result) => {
                    resolve({hotelId, result})
                })
            })
    }))
  }
  getHotel(hotelId, options){
    return new Promise((resolve => {
        const s3Gate = new S3Gate({keyName: ['hotel/', hotelId, '.json'].join('')});
        s3Gate.getObject((result) => {
            resolve(result)
        })
    }))
   }
  async getFloorDailyRatesFromOTA(websiteURL) {
    const cookie = {
        name: 'cticket',
        value: '60FA278C323F0C3ECEFD3A99331816699BBA21A7E941AC77D46F9373DB253462',
        domain: '.ctrip.com',
        url: 'https://hotels.ctrip.com/',
        path: '/',
        httpOnly: false,
        secure: false
    };

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    // await page.setCookie(cookie)

    await page.goto(websiteURL);
    
    const cookiesPromise = page.cookies();
    cookiesPromise
      .then((res) => {
        console.log('------------------- page.cookies() -------------------', JSON.stringify(res))
      })

    console.log({websiteURL})

    const hotelNameBlock = '.detail-headline_container .detail-headline_name';
    const hotelAddressBlock = '.detail-headline_container .detail-headline_position_text';
    const hotelPriceBlock = '.detail-headline_container .detail-head-price_price';
    await page.waitForSelector(hotelNameBlock);
    await page.waitForSelector(hotelAddressBlock);
    await page.waitForSelector(hotelPriceBlock);

    const hotelName = (await page.evaluate(hotelNameBlock => {
      const anchors = Array.from(document.querySelectorAll(hotelNameBlock));
      return anchors.map(anchor => {
          const title = anchor.textContent.split('|')[0].trim();
          return `${title}`;
        });
    }, hotelNameBlock)).join('');

    const hotelAddress = (await page.evaluate(hotelAddressBlock => {
      const anchors = Array.from(document.querySelectorAll(hotelAddressBlock));
      return anchors.map(anchor => {
          const title = anchor.textContent.split('|')[0].trim();
          return `${title}`;
      });
    }, hotelAddressBlock)).join('');

    const hotelPrice = (await page.evaluate(hotelPriceBlock => {
      const anchors = Array.from(document.querySelectorAll(hotelPriceBlock));
      return anchors.map(anchor => {
          const title = anchor.textContent.split('|')[0].trim();
          return `${title}`;
      });
    }, hotelPriceBlock)).join('');

    function fetchURLParams() {
        const paramsMap = {};
        const urlClusters = websiteURL.split('?')[1].split('&');
        urlClusters.forEach((urlCluster) => {
            const [key, value] = urlCluster.split('=');
            paramsMap[key] = value;
        })
        return paramsMap;
    }


    const {hotelId, checkIn: date} = fetchURLParams();
    console.log({hotelId, hotelName, hotelPrice, hotelAddress})
    const currency = hotelPrice.includes('￥') ? 'CNY' : 'USD';
    const rate = Number(currency === 'CNY' ? hotelPrice.split(' ')[0].split('￥')[1] : hotelPrice.split('$')[1]);
    return {hotelId, date, hotelName, currency, rate, hotelAddress};
    await browser.close();
  }
}

module.exports = Home;
