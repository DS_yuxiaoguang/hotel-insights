const os = require('os');

class Monitor {
  constructor() {
  }
  getServerStatus(req, res, options){
    res.send({
      result: {
        'os.platform': os.platform()
      }
    });
  }
}

module.exports = Monitor;
