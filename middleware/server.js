const express = require('express');
const path = require('path');
const app = express();
const favicon = require('serve-favicon');
const compression = require('compression');
const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
const home = require('./routes/home');
const config = require('./config/index');

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'web')));

app.use(favicon(path.join(__dirname, 'public/favicon.ico')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/shopping', home);
require('events').EventEmitter.defaultMaxListeners = 50;

app.listen(config.server.port, () => {
  console.log('Server is listening at '+ config.server.host + ':', config.server.port)
});
