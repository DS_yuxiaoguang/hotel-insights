const express = require('express');
const puppeteer = require('puppeteer');
const router = express.Router();

const Constants = require('../models/common/Constants');
const Home = require('../models/Home');
const Monitor = require('../models/Monitor');

router.use(function timeLog(req, res, next) {
  // console.log('Time: ', Date.now());
  //logger.info('Access to : ', req.baseUrl);
  next();
});

router.get('/', (req, res) => {
  renderHomeWidget(req, res);
});

router.get('/status.ci', (req, res) => {
  const monitor = new Monitor();
  monitor.getServerStatus(req, res);
});

router.post('/searchDailyRates', (req, res) => {
    const home = new Home();
    const {websites} = req.body;
    const hotelIds = ['1618828', '22755589', '2987364', '439892', '445685'];

    Promise
        .all(hotelIds.map((hotelId) => {return home.getHotel(hotelId)}))
        .then((searchResults) => {


            res.send({result: searchResults})
        })
});

router.post('/updateDailyRates', (req, res) => {
  const home = new Home();
  const {websites} = req.body;

  Promise
      .all(websites.map((websiteURL) => {return home.getFloorDailyRatesFromOTA(websiteURL)}))
      .then((searchResults) => {
          console.log({searchResults})
          res.send(searchResults)
          // Promise
          //     .all(searchResults.map((searchResult) => {
          //       const {hotelId, date, hotelName, currency, rate, hotelAddress} = searchResult;
          //       let dailyRates = {};
          //       dailyRates[date] = {
          //           date,
          //           currency,
          //           'rate': {
          //               'amountBeforeTax': rate,
          //               'amountAfterTax': rate
          //           }
          //       }
          //       return home.updateHotel(hotelId, hotelName, hotelAddress, dailyRates)
          //     }))
          //     .then((updateHotelResults) => {
          //         res.send({updateHotelResults})
          //     })

      })
});

function renderHomeWidget(req, res){
  const home = new Home();
  home.renderHomeWidget(req, res);
}

module.exports = router;
