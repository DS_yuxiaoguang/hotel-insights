const merge = require('webpack-merge');
const { endpoint, server, configkeeper, externetService } = require('./endpoint');

const CONFIGKEEPER = require('../common/configkeeper');

module.exports = {
  endpoint,
  server: JSON.parse(server),
  configkeeper: JSON.parse(configkeeper),
  externetService: merge({
    RPCSuffixCluster: {
      0: 'OPEN',
      1: 'GO.PARTNER',
      2: 'GO.SUPPLIER',
      3: 'GO.DISTRIBUTOR',
      5: 'GO.CONNECTION',
      6: 'GO.USER',
      7: 'GO.ARI',
      8: 'GO.HOTELFEE',
      9: 'GO.UPLOAD',
      10: 'GO.INSPECTOR'
    },
    getServiceConfigKey: function(suffixCategory){
      let result = this.RPCSuffixCluster[suffixCategory];
      if(!!result){
        return "service."+result.toLowerCase();
      }
      return "service.default";
    },
    getServiceURL: function (suffixCategory, options) {
      return CONFIGKEEPER.getConfig(this.getServiceConfigKey(suffixCategory));
    }
  }, JSON.parse(externetService))
};
